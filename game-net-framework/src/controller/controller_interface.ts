import {Controller} from "controller"

const ControllerInterface = new Controller('', '', '');

export function init_window(window) {
    ControllerInterface.window = window;
    ControllerInterface.StateMachine.start();
}
export const JoinRoom = ControllerInterface.JoinRoom
export const CreateRoom = ControllerInterface.CreateRoom
export const JoinMaster = ControllerInterface.JoinMaster