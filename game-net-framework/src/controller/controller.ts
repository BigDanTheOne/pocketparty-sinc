import {ControllerMassageType, ControllerMessagesFactory} from "../common/controller_messages";
import {GameBeginsMessage, Message, MessageType, MessagesFactory} from "../common/messages";
// import game_index from './../controller/FlappyBird-JavaScript/game_index.html';

const game_index = require('./FlappyBird-JavaScript/game_index.html');

let message_factory = new MessagesFactory();

export enum State {
    INIT = "INIT",
    JOIN_USER = "JOIN_USER" ,
    CREATE_ROOM = "CREATE_ROOM",
    PARTY_BEGINS = "PARTY_BEGINS",
    GAME_BEGINS = "GAME_BEGINS",
    GAME_ENDS = "GAME_ENDS",
    PARTY_ENDS = "PARTY_ENDS",
    GAME_EVENT = "GAME_EVENT"
};

type ClientID = string;
type URI = string;

function wrong_state() {
    this.socket.send(this.controller_message_factory.MakeControllerMessage("1",
        this.id, "-2", "Current state does not match the message type"))
}

export class Controller {
    handler : string;
    id : ClientID;
    socket : WebSocket;
    room_code : string;
    qr_code : any;
    uri : URI;
    window;
    players : any;
    constructor(id_ : ClientID, uri_ : URI, window_) {
        this.id = id_;
        this.uri = uri_;
        this.window = window_;
        console.log(window_)
        for (let state_ in this.StateMachine.transitions) {
            if (typeof this.StateMachine.transitions[state_] == 'function') {
                continue;
            }
            for (let func_ in this.StateMachine.transitions[state_]) {
               if (typeof this.StateMachine.transitions[state_][func_] == 'function') {
                    this.StateMachine.transitions[state_][func_] =
                                                this.StateMachine.transitions[state_][func_].bind(this);
                }
            }

        }
    }
    StateMachine = {
        curr_state : State.INIT,
        players_number : 0,
        transitions: {
            INIT: {
                Init : function (handler: string, id: ClientID, uri_ : URI) {
                    this.socket = ''; //new WebSocket(uri_);
                    this.handler = handler;
                    this.id = id;
                    this.controller_message_factory = new ControllerMessagesFactory();
                    this.socket.onopen = function (event) {
                        //TODO: check if the stream was opened correctly
                        this.socket.send(this.controller_message_factory.MakeControllerMessage("1",
                                                                                        this.id, event.id, true))
                    };
                    this.socket.onclose = function (event) {
                        this.socket.send(this.controller_message_factory.MakeControllerMessage("1",
                            this.id, event.id, event.wasClean))
                        //alert('Код: ' + event.code + ' причина: ' + event.reason);
                    };
                    this.socket.onerror = function (error) {
                        this.socket.send(this.controller_message_factory.MakeControllerMessage("1",
                            this.id, error.id, error.message))
                    };
                    this.socket.onmessage = function (event) {
                        this.dispatch(event);
                    };
                },
            },
            JOIN_USER: {
                Run: function(message) {
                    console.log("on JOIN_USER");
                    //console.log(this.socket);
                    if (message.type == MessageType.NewRoomMessage) {
                        //this.socket = message.data
                        //this.qr_code = ...
                        //this.room_code = ...
                        // TODO: establish connection, set qr_code, room_code
                        this.changeState(State.PARTY_BEGINS)
                    }
                    else {
                        wrong_state();
                    }
                }
            },
            CREATE_ROOM: {
                Run: function(message) {
                    console.log("on CREATE_ROOM");
                    //console.log(this.socket);
                    if (message.type == MessageType.RoomIdMessage) {
                        //this.socket = message.data. ...
                        //this.qr_code = ...
                        //this.room_code = ...
                        // TODO: establish connection, set qr_code, room_code
                    }
                    else if  (message.type == MessageType.UserJoinedMessage) {
                        //TODO: add players to this.players
                    }
                    else {
                        wrong_state();
                    }
                }
            },
            PARTY_BEGINS: {
                Run: function(message) {
                    console.log("on PARTY_BEGINS");
                    if  (message.type == MessageType.UserJoinedMessage) {
                        //TODO: add players to this.players
                    }
                    else if (message.type == MessageType.GameBeginsMassage) {
                        this.StateMachine.changeState(State.GAME_BEGINS);
                    }
                    else {
                        wrong_state();
                    }
                }
            },
            GAME_BEGINS: {
                Run: function(message) {
                    console.log("on GAME_BEGINS");
                    console.log(this.window);
                    let iframe = this.window.document.createElement('iframe');
                    //var html = '../controller/FlappyBird-JavaScript/game_index.html';
                    iframe.src = game_index;
                    this.window.document.body.appendChild(iframe);

                }
            },
            GAME_EVENT: {
                Run: function(message) {
                    console.log("on GAME_EVENT");
                    if (message.type == MessageType.GameEventMessage) {
                        this.StateMachine.changeState(State.GAME_EVENT);
                    }
                    else if (message.type == MessageType.GameEndsMessage) {
                        this.StateMachine.changeState(State.GAME_ENDS);
                    }
                    else {
                        wrong_state();
                    }


                    let ifrm = this.window.document.createElement("iframe");
                    ifrm.setAttribute("src", "message.data.src");
                    ifrm.style.width = "640px";
                    ifrm.style.height = "480px";
                    document.body.appendChild(ifrm);
                }
            },
            GAME_ENDS: {
                Run: function(message) {
                    console.log("on GAME_ENDS");
                    if (message.type == MessageType.PartyEndsMessage) {
                        this.StateMachine.changeState(State.PARTY_ENDS);
                    }
                    else if (message.type == MessageType.GameBeginsMassage) {
                        this.StateMachine.changeState(State.GAME_BEGINS);
                    }
                    else {
                        wrong_state();
                    }
                }
            },
            PARTY_ENDS: {
                Run: function(message) {
                    console.log("on PARTY_ENDS");
                    if (message.type == MessageType.PartyBeginsMessage) {
                        this.StateMachine.changeState(State.PARTY_BEGINS);
                    }
                    else {
                        wrong_state();
                    }
                }
            },
        },
        dispatch(message : Message) {
            console.log("dispatching message")
            const action = this.transitions[this.curr_state]["Run"];
            action.apply(this, message);
        },

        changeState(newState : State) {
            this.curr_state = newState;
            console.log("State changed!")
        },

        start() {
            this.transitions[this.curr_state]["Init"].apply(this.StateMachine);
        }
    };

    JoinRoom(name, room_code = '', qr_code = '') {
        this.StateMachine.changeState(State.JOIN_USER)
        //this.socket = party_code; // TODO: establish connection based on party_code
        let msg = message_factory.MakeMessage(MessageType.RequestNewRoomMessage, name, room_code, qr_code)
        this.socket.send(msg.serialize())
    }

    CreateRoom() {
        this.StateMachine.changeState(State.CREATE_ROOM)
        let msg = message_factory.MakeMessage(MessageType.RequestNewRoomMessage, 'master')
        this.socket.send(msg.serialize())
    }

    JoinMaster() {
        this.StateMachine.changeState(State.PARTY_BEGINS);
    }
}
// console.log("Hello world!");
// let controller = new Controller("1", "1", '1');
// controller.StateMachine.start();
// controller.StateMachine.changeState(State.GAME_BEGINS);
// controller.StateMachine.dispatch(new GameBeginsMessage('0', "Flappy bird", 'arcade'));
