"use strict";
exports.__esModule = true;
exports.Controller = exports.State = void 0;
var messages_1 = require("../common/messages");
var controller_messages_1 = require("../common/controller_messages");
export var State;
(function (State) {
    State["INIT"] = "INIT";
    State["USER_JOINED"] = "USER_JOINED";
    State["PARTY_BEGINS"] = "PARTY_BEGINS";
    State["GAME_BEGINS"] = "GAME_BEGINS";
    State["GAME_ENDS"] = "GAME_ENDS";
    State["PARTY_ENDS"] = "PARTY_ENDS";
    State["GAME_EVENT"] = "GAME_EVENT";
})(State = exports.State || (exports.State = {}));
;
function wrong_state() {
    this.socket.send(this.controller_message_factory.MakeControllerMessage("1", this.id, "-2", "Current state does not match the message type"));
}
export var Controller = /** @class */ (function () {
    function Controller(id_, uri_, window_) {
        this.StateMachine = {
            curr_state: State.INIT,
            players_number: 0,
            transitions: {
                INIT: {
                    Init: function (handler, id, uri_) {
                        this.socket = {}; //new WebSocket(uri_);
                        this.handler = handler;
                        this.id = id;
                        this.controller_message_factory = new controller_messages_1.ControllerMessagesFactory();
                        this.changeState(State.USER_JOINED);
                        //this.dispatch("Run");
                        this.socket.onopen = function (event) {
                            //TODO: check if the stream was opened correctly
                            this.socket.send(this.controller_message_factory.MakeControllerMessage("1", this.id, event.id, true));
                        };
                        this.socket.onclose = function (event) {
                            this.socket.send(this.controller_message_factory.MakeControllerMessage("1", this.id, event.id, event.wasClean));
                            //alert('Код: ' + event.code + ' причина: ' + event.reason);
                        };
                        this.socket.onerror = function (error) {
                            this.socket.send(this.controller_message_factory.MakeControllerMessage("1", this.id, error.id, error.message));
                        };
                        this.socket.onmessage = function (event) {
                            this.dispatch(event);
                        };
                    },
                    Run: function (message) {
                        if (message.type != messages_1.MassegeType.UserJoinedMessage) {
                            wrong_state();
                        }
                        else {
                            this.changeState(State.USER_JOINED);
                        }
                    }
                },
                USER_JOINED: {
                    Run: function (message) {
                        console.log("on USER_JOINED");
                        console.log(this.socket);
                        if (message.type == messages_1.MassegeType.UserJoinedMessage) {
                            //
                        }
                        else if (message.type == messages_1.MassegeType.GameBeginsMassage) {
                            this.changeState(State.GAME_BEGINS);
                        }
                        else {
                            wrong_state();
                        }
                    }
                },
                PARTY_BEGINS: {
                    Run: function (message) {
                        console.log("on PARTY_BEGINS");
                        if (message.type == messages_1.MassegeType.GameBeginsMassage) {
                            this.changeState(State.PARTY_BEGINS);
                        }
                        else {
                            wrong_state();
                        }
                    }
                },
                GAME_EVENT: {
                    Run: function (message) {
                        console.log("on GAME_EVENT");
                        if (message.type == messages_1.MassegeType.GameEventMessage) {
                            this.changeState(State.GAME_EVENT);
                        }
                        else if (message.type == messages_1.MassegeType.GameEndsMessage) {
                            this.changeState(State.GAME_ENDS);
                        }
                        else {
                            wrong_state();
                        }
                        var ifrm = this.window.createElement("iframe");
                        ifrm.setAttribute("src", "message.data.src");
                        ifrm.style.width = "640px";
                        ifrm.style.height = "480px";
                        document.body.appendChild(ifrm);
                    }
                },
                GAME_ENDS: {
                    Run: function (message) {
                        console.log("on GAME_ENDS");
                        if (message.type == messages_1.MassegeType.PartyEndsMessage) {
                            this.changeState(State.PARTY_ENDS);
                        }
                        else if (message.type == messages_1.MassegeType.GameBeginsMassage) {
                            this.changeState(State.GAME_BEGINS);
                        }
                        else {
                            wrong_state();
                        }
                    }
                },
                PARTY_ENDS: {
                    Run: function (message) {
                        console.log("on PARTY_ENDS");
                        if (message.type == messages_1.MassegeType.PartyBeginsMessage) {
                            this.changeState(State.PARTY_BEGINS);
                        }
                        else {
                            wrong_state();
                        }
                    }
                }
            },
            dispatch: function (message) {
                var action = this.transitions[this.curr_state]["Run"];
                action.apply(this.StateMachine, message);
            },
            changeState: function (newState) {
                this.curr_state = newState;
                console.log("State changed!");
            },
            start: function () {
                this.transitions[this.curr_state]["Init"].apply(this.StateMachine);
            }
        };
        this.id = id_;
        this.uri = uri_;
        this.window = window_;
        for (var state_ in this.StateMachine.transitions) {
            if (typeof this.StateMachine.transitions[state_] == 'function') {
                continue;
            }
            for (var func_ in this.StateMachine.transitions[state_]) {
                if (typeof this.StateMachine.transitions[state_][func_] == 'function') {
                    this.StateMachine.transitions[state_][func_] =
                        this.StateMachine.transitions[state_][func_].bind(this.StateMachine);
                }
            }
        }
    }
    return Controller;
}());
exports.Controller = Controller;
console.log("Hello world!");
//let controller = new Controller("1", "1", "1");
//controller.StateMachine.start();
// controller.StateMachine.changeState(State.GAME_BEGINS);
// controller.StateMachine.dispatch(new GameBeginsMessage('0', "Flappy bird", 'arcade', "FlappyBird-JavaScript/game_index.html"));
