"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.ControllerMessagesFactory = exports.OnMessageControllerMessage = exports.OnErrorControllerMessage = exports.OnCloseControllerMessage = exports.OnOpenControllerMessage = exports.ControllerMessage = exports.ControllerMassageType = void 0;
var ControllerMassageType;
(function (ControllerMassageType) {
    ControllerMassageType[ControllerMassageType["OnErrorType"] = 0] = "OnErrorType";
    ControllerMassageType[ControllerMassageType["OnCloseType"] = 1] = "OnCloseType";
    ControllerMassageType[ControllerMassageType["OnOpenType"] = 2] = "OnOpenType";
    ControllerMassageType[ControllerMassageType["OnMessageType"] = 3] = "OnMessageType";
})(ControllerMassageType = exports.ControllerMassageType || (exports.ControllerMassageType = {}));
var ControllerMessage = /** @class */ (function () {
    function ControllerMessage(id_, user_, input_message_id_, type_) {
        this.id = id_;
        this.user = user_;
        this.input_message_id = input_message_id_;
        this.type = type_;
    }
    ControllerMessage.prototype.serialize = function () {
        //
    };
    return ControllerMessage;
}());
exports.ControllerMessage = ControllerMessage;
var OnOpenControllerMessage = /** @class */ (function (_super) {
    __extends(OnOpenControllerMessage, _super);
    function OnOpenControllerMessage(id_, user_, input_message_id_, was_successful_) {
        var _this = _super.call(this, id_, user_, input_message_id_, ControllerMassageType.OnOpenType) || this;
        _this.type = ControllerMassageType.OnOpenType;
        _this.was_successful = was_successful_;
        return _this;
    }
    return OnOpenControllerMessage;
}(ControllerMessage));
exports.OnOpenControllerMessage = OnOpenControllerMessage;
var OnCloseControllerMessage = /** @class */ (function (_super) {
    __extends(OnCloseControllerMessage, _super);
    function OnCloseControllerMessage(id_, user_, input_message_id_, was_clean_) {
        var _this = _super.call(this, id_, user_, input_message_id_, ControllerMassageType.OnCloseType) || this;
        _this.type = ControllerMassageType.OnCloseType;
        _this.was_clean = was_clean_;
        return _this;
    }
    return OnCloseControllerMessage;
}(ControllerMessage));
exports.OnCloseControllerMessage = OnCloseControllerMessage;
var OnErrorControllerMessage = /** @class */ (function (_super) {
    __extends(OnErrorControllerMessage, _super);
    function OnErrorControllerMessage(id_, user_, input_message_id_, error_) {
        var _this = _super.call(this, id_, user_, input_message_id_, ControllerMassageType.OnErrorType) || this;
        _this.type = ControllerMassageType.OnCloseType;
        _this.error = error_;
        return _this;
    }
    return OnErrorControllerMessage;
}(ControllerMessage));
exports.OnErrorControllerMessage = OnErrorControllerMessage;
var OnMessageControllerMessage = /** @class */ (function (_super) {
    __extends(OnMessageControllerMessage, _super);
    function OnMessageControllerMessage(id_, user_, input_message_id_, message_) {
        var _this = _super.call(this, id_, user_, input_message_id_, ControllerMassageType.OnMessageType) || this;
        _this.type = ControllerMassageType.OnCloseType;
        _this.message = message_;
        return _this;
    }
    return OnMessageControllerMessage;
}(ControllerMessage));
exports.OnMessageControllerMessage = OnMessageControllerMessage;
var ControllerMessagesFactory = /** @class */ (function () {
    function ControllerMessagesFactory() {
        this.counter = 0;
    }
    ControllerMessagesFactory.prototype.MakeControllerMessage = function (new_messege_type, user_, input_message_id_, custom) {
        // implement more complex logic of index incrementing
        //add assertions (may be they are included in typescript compiler)
        switch (new_messege_type) {
            case ControllerMassageType.OnOpenType: {
                return new OnOpenControllerMessage(String(this.counter++), user_, input_message_id_, custom);
            }
            case ControllerMassageType.OnCloseType: {
                return new OnCloseControllerMessage(String(this.counter++), user_, input_message_id_, custom);
            }
            case ControllerMassageType.OnErrorType: {
                return new OnErrorControllerMessage(String(this.counter++), user_, input_message_id_, custom);
            }
            case ControllerMassageType.OnMessageType: {
                return new OnMessageControllerMessage(String(this.counter++), user_, input_message_id_, custom);
            }
        }
    };
    return ControllerMessagesFactory;
}());
exports.ControllerMessagesFactory = ControllerMessagesFactory;
