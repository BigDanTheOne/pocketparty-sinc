import { UserJoinedMessage } from "./messages";
import { UserID } from "./user";

export type Scores = Map<UserID, number>;