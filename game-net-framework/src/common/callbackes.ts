import {IGameEvent} from "./events";
import {Scores} from "./scores";

export interface eventReceiver {
    (e: IGameEvent): void;
}

export interface gameCloser {
    (s: Scores): void;
}