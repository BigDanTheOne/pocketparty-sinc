export type UserID = number;

export class User {
    id: UserID;
    handle: string;
}