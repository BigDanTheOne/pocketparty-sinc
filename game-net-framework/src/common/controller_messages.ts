import { User } from './user';
import {Message,
    UserJoinedMessage,
    PartyBeginsMessage,
    GameBeginsMessage,
    GameEndsMessage,
    PartyEndsMessage,
    GameEventsMessage} from "../common/messages";

export enum ControllerMassageType {
    OnErrorType,
    OnCloseType,
    OnOpenType,
    OnMessageType
}

export type IDType = string;

export class ControllerMessage {
    id : IDType;
    user : User;
    input_message_id : IDType;
    type: ControllerMassageType;

    constructor(id_ : IDType, user_ : User, input_message_id_ : IDType, type_ : ControllerMassageType ) {
        this.id = id_;
        this.user = user_;
        this.input_message_id = input_message_id_;
        this.type = type_;
    }
    public serialize () : void {
        //
    }
}

export class OnOpenControllerMessage extends ControllerMessage {
    type : ControllerMassageType = ControllerMassageType.OnOpenType;
    user : User;
    input_message_id : IDType;
    was_successful : boolean;

    constructor(id_ : IDType, user_ : User, input_message_id_ : IDType, was_successful_ : boolean) {
        super(id_, user_, input_message_id_, ControllerMassageType.OnOpenType);
        this.was_successful = was_successful_;
    }
}

export class OnCloseControllerMessage extends ControllerMessage {
    type : ControllerMassageType = ControllerMassageType.OnCloseType;
    user : User;
    input_message_id : IDType;
    was_clean : boolean;

    constructor(id_ : IDType, user_ : User, input_message_id_ : IDType,  was_clean_ : boolean) {
        super(id_, user_, input_message_id_, ControllerMassageType.OnCloseType);
        this.was_clean = was_clean_;
    }
}

export class OnErrorControllerMessage extends ControllerMessage {
    type : ControllerMassageType = ControllerMassageType.OnCloseType;
    user : User;
    input_message_id : IDType;
    error : string;

    constructor(id_ : IDType, user_ : User, input_message_id_ : IDType, error_ : string) {
        super(id_, user_, input_message_id_, ControllerMassageType.OnErrorType);
        this.error = error_;
    }
}

export class OnMessageControllerMessage extends ControllerMessage {
    type : ControllerMassageType = ControllerMassageType.OnCloseType;
    user : User;
    input_message_id : IDType;
    message: Message;

    constructor(id_ : IDType, user_ : User, input_message_id_ : IDType, message_ : Message) {
        super(id_, user_, input_message_id_, ControllerMassageType.OnMessageType);
        this.message = message_;

    }
}

export class ControllerMessagesFactory{
    counter : number
    constructor() {
        this.counter = 0;
    }
    public MakeControllerMessage(new_messege_type : ControllerMassageType, user_ : User,
                                 input_message_id_ : IDType, custom : any) : ControllerMessage {
        // implement more complex logic of index incrementing
        //add assertions (may be they are included in typescript compiler)
        switch(new_messege_type) {
            case ControllerMassageType.OnOpenType : {
                return new OnOpenControllerMessage(String(this.counter++), user_, input_message_id_, custom)
            }
            case ControllerMassageType.OnCloseType : {
                return new OnCloseControllerMessage(String(this.counter++), user_, input_message_id_, custom)
            }
            case ControllerMassageType.OnErrorType : {
                return new OnErrorControllerMessage(String(this.counter++), user_, input_message_id_, custom)
            }
            case ControllerMassageType.OnMessageType : {
                return new OnMessageControllerMessage(String(this.counter++), user_, input_message_id_, custom)
            }
        }
    }
}