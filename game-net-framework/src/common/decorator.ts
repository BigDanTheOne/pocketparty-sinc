import {EventHandler, EventTag} from "./events";
import {BServer} from "../game-server/server";
import {BClient} from "../game-client/client";

export function event(tag: EventTag, allow_overwriting : boolean = false) {
    return function (target: BServer | BClient, propertyKey: string, descriptor: PropertyDescriptor) {
        if (target.__events__ === undefined)
            target.__events__ = new Map<EventTag, EventHandler>();
        if (target.__events__.get(tag) === undefined) {
            target.__events__.set(tag, descriptor.value);
        }
        else if (allow_overwriting == true) {
            target.__events__.set(tag, descriptor.value);
        }
        else {
            throw `verwriting event tag ${tag} handler is forbidden
            (set allow_overwriting = true if you want to overwrite event tag handler)`
        }
        return descriptor;
    }
}
