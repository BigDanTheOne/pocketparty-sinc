import { User } from './user';

export enum MessageType {
    UserJoinedMessage ,
    PartyBeginsMessage ,
    GameBeginsMassage,
    GameEndsMessage ,
    PartyEndsMessage ,
    GameEventMessage,
    RequestNewRoomMessage,
    RoomIdMessage,
    NewRoomMessage
};

export type IDType = string;

export class Message {
    id : IDType;
    type: MessageType;
    constructor(id_ : IDType) {
        this.id = id_;
    }
    public serialize () : string {
        return ''                   //TODO: implemet serialization
    }
    deserialize () : Message {
        return new Message(this.id) //TODO: implemet deserialization
    }
}

export class UserJoinedMessage extends Message {
    type : MessageType = MessageType.UserJoinedMessage;
    user : User

    constructor(id_ : IDType, user_ : User) {
        super(id_);
        this.user = user_;
    }
}

export class PartyBeginsMessage extends Message {
    type : MessageType = MessageType.PartyBeginsMessage;

    constructor(id_ : IDType) {
        super(id_);
    }
}

export class GameBeginsMessage extends Message {
    type : MessageType = MessageType.GameBeginsMassage;
    title : string
    game_type_id : string

    constructor(id_ : IDType, title_ : string, game_type_id_ : string) {
        super(id_);
        this.title = title_;
        this.game_type_id = game_type_id_;
    }
}

export class GameEndsMessage extends Message {
    type : MessageType = MessageType.GameEndsMessage;
    scores : Map<IDType, number>

    constructor(id_ : IDType, scores_ : Map<IDType, number>) {
        super(id_);
        this.scores = scores_;
    }
}

export class PartyEndsMessage extends Message {
    type : MessageType = MessageType.PartyEndsMessage;
    scores : Map<IDType, number>

    constructor(id_ : IDType, scores_ : Map<IDType, number>) {
        super(id_);
        this.scores = scores_;
    }
}

export class GameEventsMessage extends Message {
    scores : Map<IDType, number>
}

export class RequestNewRoomMessage extends Message {
    player : string;

    constructor(id_ : IDType, player_ : string) {
        super(id_);
        this.player = player_;
    }
}

export class RoomIdMessage extends Message {
    player : string;
    room_code : string;
    qr_code : string;
    web_socket_uri : string
    constructor(id_ : IDType, player_ : string, room_code_ = '', qr_code_ = '', web_socket_uri_ : string) {
        super(id_);
        this.player = player_;
        this.room_code = room_code_;
        this.qr_code = qr_code_;
        this.web_socket_uri = web_socket_uri_;
    }
}

export class NewRoomMessage extends Message {
    web_socket_uri : string
    constructor(id_ : IDType, web_socket_uri_ : string) {
        super(id_);
        this.web_socket_uri = web_socket_uri_;
    }
}
export class MessagesFactory{
    counter : number
    constructor() {
        this.counter = 0;
    }
    public MakeMessage(new_messege_type : MessageType, ...argv) : Message {
        // implement more complex logic of index incrementing
        //add assertions (may be they are included in typescript compiler)
        switch(new_messege_type) {
            case MessageType.UserJoinedMessage : {
                return new UserJoinedMessage(String(this.counter++), argv[0])
            }
            case MessageType.GameBeginsMassage : {
                return new GameBeginsMessage(String(this.counter++), argv[0], argv[1])
            }
            case MessageType.PartyBeginsMessage : {
                return new PartyBeginsMessage(String(this.counter++))
            }
            case MessageType.GameEndsMessage : {
                return new GameEndsMessage(String(this.counter++), argv[0])
            }
            case MessageType.PartyEndsMessage : {
                return new PartyEndsMessage(String(this.counter++), argv[0])
            }
            case MessageType.GameEventMessage : {
                return new GameEventsMessage(String(this.counter++))
            }
            case MessageType.RequestNewRoomMessage : {
                return new RequestNewRoomMessage(String(this.counter++), argv[0])
            }
            case MessageType.NewRoomMessage: {
                return new NewRoomMessage(String(this.counter++), argv[0])
            }
            case MessageType.RoomIdMessage: {
                return new RoomIdMessage(String(this.counter++), argv[0], argv[1], argv[2], argv[3])
            }
        }
    }
}
