export type EventTag = string;

export interface IGameEvent {
    tag: EventTag;
    broadcast?: boolean;
}

export interface EventHandler extends Function {
    (event: IGameEvent): void;
}