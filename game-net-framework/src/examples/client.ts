import { BClient,  wireClient} from '../game-client/client'
import { IGameEvent, EventTag } from '../common/events'
import {event} from "../common/decorator"

class SubmitImposter implements IGameEvent {
    tag: EventTag = 'simposter';
}

class ImposterClient extends BClient {

    constructor() {
        super("id123455")
    }

    init(): void {
        this.feed({tag: "cock"});
        this.feed({tag: 'hello'});
    }
    @event("simposter")
    public on_enemy_move(event: SubmitImposter): void {
        console.log("simposter called")
    }

    @event("please-close")
    public please_close(event: IGameEvent): void {
        var result = new Map();
        result.set(10, 100);
        this.end(result);
    }

    public not_event(event: IGameEvent): void {}
}

const client = wireClient(
    (u) => {console.log(u)},
    (u) => {console.log(u)},
    ImposterClient
);

client.trigger({tag: "simposter"});
client.trigger({tag: "please-close"});