import { BServer,  wireServer} from '../game-server/server'
import { IGameEvent, EventTag } from '../common/events'
import {event} from "../common/decorator"

class SubmitImposter implements IGameEvent {
    tag: EventTag = 'simposter';
}

class ImposterServer extends BServer {
    counter: number = 0;

    constructor() {
        super()
    }

    init(): void {
        this.feed({tag: 'hello'});
    }
    @event("simposter")
    public on_enemy_move(event: SubmitImposter): void {
        console.log("simposter called")
    }

    @event("please-close")
    public please_close(event: IGameEvent): void {
        var result = new Map();
        result.set(10, 100);
        this.end(result);
    }

    public not_event(event: IGameEvent): void {}

}

const server = wireServer(
    (u) => {console.log(u)},
    (u) => {console.log(u)},
    [{handle: "cockman", id: 10}],
    ImposterServer
);

server.trigger({tag: "simposter"});
server.trigger({tag: "please-close"});