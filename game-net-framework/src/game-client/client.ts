import { EventTag, EventHandler, IGameEvent } from '../common/events'
import { User } from '../common/user'
import { Scores } from '../common/scores'
import {eventReceiver, gameCloser} from '../common/callbackes'
import {BServer} from "../game-server/server";

export type ClientID = string;
export abstract class BClient {
    __id__: ClientID;
    __events__: Map<EventTag, EventHandler>;

    // callbacks
    event_cb: eventReceiver;
    closer_cb: gameCloser;

    constructor(id: ClientID) {
        //TODO: load universal events
        //example: this.__events__[ForceExit] = force_exit_handler;
        this.__id__ = id;
    }

    public trigger(event: IGameEvent): void {
        if (this.__events__ === undefined) {
            throw 'No tagged handlers';
        }

        if (!this.__events__.has(event.tag)) {
            throw `Event tag ${event.tag} not handled`
        }

        this.__events__.get(event.tag).apply(this, [event]);
    }

    /** init is called once the game starts */
    abstract init(): void;

    /** end signals that the game has ended and returns scores */
    end(scores: Scores): void {
        this.closer_cb(scores);
    }

    /** feeds game-server events to game-client */
    protected feed(event: IGameEvent): void {
        // TODO: figure out how to run in background
        this.event_cb(event);
    }

    on(receiver: eventReceiver) {
        if (this.event_cb != undefined) {
            throw `Event handler already set: ${this.event_cb}`;
        }
        this.event_cb = receiver;
    }

    onClose(closer: gameCloser) {
        if (this.closer_cb != undefined) {
            throw `Closer already set: ${this.closer_cb}`;
        }
        this.closer_cb = closer;
    }
}


export function wireClient<SS extends BClient>
(eventCallback: eventReceiver,
 closerCallback: gameCloser,
 type: {new(): SS}): SS {
    var client = new type();

    client.on(eventCallback);
    client.onClose(closerCallback);

    client.init();

    return client;
}
