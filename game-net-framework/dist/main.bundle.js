/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/controller/FlappyBird-JavaScript/game_index.html":
/*!**************************************************************!*\
  !*** ./src/controller/FlappyBird-JavaScript/game_index.html ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _node_modules_html_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../node_modules/html-loader/dist/runtime/getUrl.js */ \"./node_modules/html-loader/dist/runtime/getUrl.js\");\n/* harmony import */ var _node_modules_html_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_html_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_0__);\n// Imports\n\nvar ___HTML_LOADER_IMPORT_0___ = new URL(/* asset import */ __webpack_require__(/*! ./flappyBird.js */ \"./src/controller/FlappyBird-JavaScript/flappyBird.js\"), __webpack_require__.b);\n// Module\nvar ___HTML_LOADER_REPLACEMENT_0___ = _node_modules_html_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_0___default()(___HTML_LOADER_IMPORT_0___);\nvar code = \"<!DOCTYPE html>\\n<html>\\n  <head>\\n    <title>Flappy Bird</title>\\n  </head>\\n  <body>\\n   <canvas id=\\\"canvas\\\" width=\\\"288\\\" height=\\\"512\\\"></canvas>\\n   <script src = \\\"\" + ___HTML_LOADER_REPLACEMENT_0___ + \"\\\"></script>\\n  </body>\\n</html>\";\n// Exports\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (code);\n\n//# sourceURL=webpack://@pocket-party/game-framework/./src/controller/FlappyBird-JavaScript/game_index.html?");

/***/ }),

/***/ "./node_modules/html-loader/dist/runtime/getUrl.js":
/*!*********************************************************!*\
  !*** ./node_modules/html-loader/dist/runtime/getUrl.js ***!
  \*********************************************************/
/***/ ((module) => {

eval("\n\nmodule.exports = function (url, options) {\n  if (!options) {\n    // eslint-disable-next-line no-param-reassign\n    options = {};\n  }\n\n  if (!url) {\n    return url;\n  } // eslint-disable-next-line no-underscore-dangle, no-param-reassign\n\n\n  url = String(url.__esModule ? url.default : url);\n\n  if (options.hash) {\n    // eslint-disable-next-line no-param-reassign\n    url += options.hash;\n  }\n\n  if (options.maybeNeedQuotes && /[\\t\\n\\f\\r \"'=<>`]/.test(url)) {\n    return \"\\\"\".concat(url, \"\\\"\");\n  }\n\n  return url;\n};\n\n//# sourceURL=webpack://@pocket-party/game-framework/./node_modules/html-loader/dist/runtime/getUrl.js?");

/***/ }),

/***/ "./src/common/controller_messages.ts":
/*!*******************************************!*\
  !*** ./src/common/controller_messages.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"ControllerMassageType\": () => (/* binding */ ControllerMassageType),\n/* harmony export */   \"ControllerMessage\": () => (/* binding */ ControllerMessage),\n/* harmony export */   \"OnOpenControllerMessage\": () => (/* binding */ OnOpenControllerMessage),\n/* harmony export */   \"OnCloseControllerMessage\": () => (/* binding */ OnCloseControllerMessage),\n/* harmony export */   \"OnErrorControllerMessage\": () => (/* binding */ OnErrorControllerMessage),\n/* harmony export */   \"OnMessageControllerMessage\": () => (/* binding */ OnMessageControllerMessage),\n/* harmony export */   \"ControllerMessagesFactory\": () => (/* binding */ ControllerMessagesFactory)\n/* harmony export */ });\nvar ControllerMassageType;\n(function (ControllerMassageType) {\n    ControllerMassageType[ControllerMassageType[\"OnErrorType\"] = 0] = \"OnErrorType\";\n    ControllerMassageType[ControllerMassageType[\"OnCloseType\"] = 1] = \"OnCloseType\";\n    ControllerMassageType[ControllerMassageType[\"OnOpenType\"] = 2] = \"OnOpenType\";\n    ControllerMassageType[ControllerMassageType[\"OnMessageType\"] = 3] = \"OnMessageType\";\n})(ControllerMassageType || (ControllerMassageType = {}));\nclass ControllerMessage {\n    constructor(id_, user_, input_message_id_, type_) {\n        this.id = id_;\n        this.user = user_;\n        this.input_message_id = input_message_id_;\n        this.type = type_;\n    }\n    serialize() {\n        //\n    }\n}\nclass OnOpenControllerMessage extends ControllerMessage {\n    constructor(id_, user_, input_message_id_, was_successful_) {\n        super(id_, user_, input_message_id_, ControllerMassageType.OnOpenType);\n        this.type = ControllerMassageType.OnOpenType;\n        this.was_successful = was_successful_;\n    }\n}\nclass OnCloseControllerMessage extends ControllerMessage {\n    constructor(id_, user_, input_message_id_, was_clean_) {\n        super(id_, user_, input_message_id_, ControllerMassageType.OnCloseType);\n        this.type = ControllerMassageType.OnCloseType;\n        this.was_clean = was_clean_;\n    }\n}\nclass OnErrorControllerMessage extends ControllerMessage {\n    constructor(id_, user_, input_message_id_, error_) {\n        super(id_, user_, input_message_id_, ControllerMassageType.OnErrorType);\n        this.type = ControllerMassageType.OnCloseType;\n        this.error = error_;\n    }\n}\nclass OnMessageControllerMessage extends ControllerMessage {\n    constructor(id_, user_, input_message_id_, message_) {\n        super(id_, user_, input_message_id_, ControllerMassageType.OnMessageType);\n        this.type = ControllerMassageType.OnCloseType;\n        this.message = message_;\n    }\n}\nclass ControllerMessagesFactory {\n    constructor() {\n        this.counter = 0;\n    }\n    MakeControllerMessage(new_messege_type, user_, input_message_id_, custom) {\n        // implement more complex logic of index incrementing\n        //add assertions (may be they are included in typescript compiler)\n        switch (new_messege_type) {\n            case ControllerMassageType.OnOpenType: {\n                return new OnOpenControllerMessage(String(this.counter++), user_, input_message_id_, custom);\n            }\n            case ControllerMassageType.OnCloseType: {\n                return new OnCloseControllerMessage(String(this.counter++), user_, input_message_id_, custom);\n            }\n            case ControllerMassageType.OnErrorType: {\n                return new OnErrorControllerMessage(String(this.counter++), user_, input_message_id_, custom);\n            }\n            case ControllerMassageType.OnMessageType: {\n                return new OnMessageControllerMessage(String(this.counter++), user_, input_message_id_, custom);\n            }\n        }\n    }\n}\n\n\n//# sourceURL=webpack://@pocket-party/game-framework/./src/common/controller_messages.ts?");

/***/ }),

/***/ "./src/common/messages.ts":
/*!********************************!*\
  !*** ./src/common/messages.ts ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"MessageType\": () => (/* binding */ MessageType),\n/* harmony export */   \"Message\": () => (/* binding */ Message),\n/* harmony export */   \"UserJoinedMessage\": () => (/* binding */ UserJoinedMessage),\n/* harmony export */   \"PartyBeginsMessage\": () => (/* binding */ PartyBeginsMessage),\n/* harmony export */   \"GameBeginsMessage\": () => (/* binding */ GameBeginsMessage),\n/* harmony export */   \"GameEndsMessage\": () => (/* binding */ GameEndsMessage),\n/* harmony export */   \"PartyEndsMessage\": () => (/* binding */ PartyEndsMessage),\n/* harmony export */   \"GameEventsMessage\": () => (/* binding */ GameEventsMessage)\n/* harmony export */ });\nvar MessageType;\n(function (MessageType) {\n    MessageType[MessageType[\"UserJoinedMessage\"] = 0] = \"UserJoinedMessage\";\n    MessageType[MessageType[\"PartyBeginsMessage\"] = 1] = \"PartyBeginsMessage\";\n    MessageType[MessageType[\"GameBeginsMassage\"] = 2] = \"GameBeginsMassage\";\n    MessageType[MessageType[\"GameEndsMessage\"] = 3] = \"GameEndsMessage\";\n    MessageType[MessageType[\"PartyEndsMessage\"] = 4] = \"PartyEndsMessage\";\n    MessageType[MessageType[\"GameEventMessage\"] = 5] = \"GameEventMessage\";\n})(MessageType || (MessageType = {}));\n;\nclass Message {\n    constructor(id_) {\n        this.id = id_;\n    }\n    serialize() {\n        //\n    }\n}\nclass UserJoinedMessage extends Message {\n    constructor(id_, user_) {\n        super(id_);\n        this.type = MessageType.UserJoinedMessage;\n        this.user = user_;\n    }\n}\nclass PartyBeginsMessage extends Message {\n    constructor(id_) {\n        super(id_);\n        this.type = MessageType.PartyBeginsMessage;\n    }\n}\nclass GameBeginsMessage extends Message {\n    constructor(id_, title_, game_type_id_) {\n        super(id_);\n        this.type = MessageType.GameBeginsMassage;\n        this.title = title_;\n        this.game_type_id = game_type_id_;\n    }\n}\nclass GameEndsMessage extends Message {\n    constructor(id_, scores_) {\n        super(id_);\n        this.type = MessageType.GameEndsMessage;\n        this.scores = scores_;\n    }\n}\nclass PartyEndsMessage extends Message {\n    constructor(id_, scores_) {\n        super(id_);\n        this.type = MessageType.PartyEndsMessage;\n        this.scores = scores_;\n    }\n}\nclass GameEventsMessage extends Message {\n}\nclass MessagesFactory {\n    constructor() {\n        this.counter = 0;\n    }\n    MakeMessage(new_messege_type, ...argv) {\n        // implement more complex logic of index incrementing\n        //add assertions (may be they are included in typescript compiler)\n        switch (new_messege_type) {\n            case MessageType.UserJoinedMessage: {\n                return new UserJoinedMessage(String(this.counter++), argv[0]);\n            }\n            case MessageType.GameBeginsMassage: {\n                return new GameBeginsMessage(String(this.counter++), argv[0], argv[1]);\n            }\n            case MessageType.PartyBeginsMessage: {\n                return new PartyBeginsMessage(String(this.counter++));\n            }\n            case MessageType.GameEndsMessage: {\n                return new GameEndsMessage(String(this.counter++), argv[0]);\n            }\n            case MessageType.PartyEndsMessage: {\n                return new PartyEndsMessage(String(this.counter++), argv[0]);\n            }\n            case MessageType.GameEventMessage: {\n                return new GameEventsMessage(String(this.counter++));\n            }\n        }\n    }\n}\n\n\n//# sourceURL=webpack://@pocket-party/game-framework/./src/common/messages.ts?");

/***/ }),

/***/ "./src/controller/controller.ts":
/*!**************************************!*\
  !*** ./src/controller/controller.ts ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"State\": () => (/* binding */ State),\n/* harmony export */   \"Controller\": () => (/* binding */ Controller)\n/* harmony export */ });\n/* harmony import */ var _common_controller_messages__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/controller_messages */ \"./src/common/controller_messages.ts\");\n/* harmony import */ var _common_messages__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/messages */ \"./src/common/messages.ts\");\n\n\n// import game_index from './../controller/FlappyBird-JavaScript/game_index.html';\nconst game_index = __webpack_require__(/*! ./FlappyBird-JavaScript/game_index.html */ \"./src/controller/FlappyBird-JavaScript/game_index.html\");\n;\nvar State;\n(function (State) {\n    State[\"INIT\"] = \"INIT\";\n    State[\"USER_JOINED\"] = \"USER_JOINED\";\n    State[\"PARTY_BEGINS\"] = \"PARTY_BEGINS\";\n    State[\"GAME_BEGINS\"] = \"GAME_BEGINS\";\n    State[\"GAME_ENDS\"] = \"GAME_ENDS\";\n    State[\"PARTY_ENDS\"] = \"PARTY_ENDS\";\n    State[\"GAME_EVENT\"] = \"GAME_EVENT\";\n})(State || (State = {}));\n;\nfunction wrong_state() {\n    this.socket.send(this.controller_message_factory.MakeControllerMessage(\"1\", this.id, \"-2\", \"Current state does not match the message type\"));\n}\nclass Controller {\n    constructor(id_, uri_, window_) {\n        this.StateMachine = {\n            curr_state: State.INIT,\n            players_number: 0,\n            transitions: {\n                INIT: {\n                    Init: function (handler, id, uri_) {\n                        this.socket = {}; //new WebSocket(uri_);\n                        this.handler = handler;\n                        this.id = id;\n                        this.controller_message_factory = new _common_controller_messages__WEBPACK_IMPORTED_MODULE_0__.ControllerMessagesFactory();\n                        this.StateMachine.changeState(State.USER_JOINED);\n                        //this.dispatch(\"Run\");\n                        this.socket.onopen = function (event) {\n                            //TODO: check if the stream was opened correctly\n                            this.socket.send(this.controller_message_factory.MakeControllerMessage(\"1\", this.id, event.id, true));\n                        };\n                        this.socket.onclose = function (event) {\n                            this.socket.send(this.controller_message_factory.MakeControllerMessage(\"1\", this.id, event.id, event.wasClean));\n                            //alert('Код: ' + event.code + ' причина: ' + event.reason);\n                        };\n                        this.socket.onerror = function (error) {\n                            this.socket.send(this.controller_message_factory.MakeControllerMessage(\"1\", this.id, error.id, error.message));\n                        };\n                        this.socket.onmessage = function (event) {\n                            this.dispatch(event);\n                        };\n                    },\n                    Run: function (message) {\n                        if (message.type != _common_messages__WEBPACK_IMPORTED_MODULE_1__.MessageType.UserJoinedMessage) {\n                            wrong_state();\n                        }\n                        else {\n                            this.changeState(State.USER_JOINED);\n                        }\n                    }\n                },\n                USER_JOINED: {\n                    Run: function (message) {\n                        console.log(\"on USER_JOINED\");\n                        console.log(this.socket);\n                        if (message.type == _common_messages__WEBPACK_IMPORTED_MODULE_1__.MessageType.UserJoinedMessage) {\n                            //\n                        }\n                        else if (message.type == _common_messages__WEBPACK_IMPORTED_MODULE_1__.MessageType.GameBeginsMassage) {\n                            this.StateMachine.changeState(State.GAME_BEGINS);\n                        }\n                        else {\n                            wrong_state();\n                        }\n                    }\n                },\n                PARTY_BEGINS: {\n                    Run: function (message) {\n                        console.log(\"on PARTY_BEGINS\");\n                        if (message.type == _common_messages__WEBPACK_IMPORTED_MODULE_1__.MessageType.GameBeginsMassage) {\n                            this.StateMachine.changeState(State.GAME_BEGINS);\n                        }\n                        else {\n                            wrong_state();\n                        }\n                    }\n                },\n                GAME_BEGINS: {\n                    Run: function (message) {\n                        console.log(\"on GAME_BEGINS\");\n                        console.log(this.window);\n                        let iframe = this.window.document.createElement('iframe');\n                        //var html = '../controller/FlappyBird-JavaScript/game_index.html';\n                        iframe.src = game_index;\n                        this.window.document.body.appendChild(iframe);\n                    }\n                },\n                GAME_EVENT: {\n                    Run: function (message) {\n                        console.log(\"on GAME_EVENT\");\n                        if (message.type == _common_messages__WEBPACK_IMPORTED_MODULE_1__.MessageType.GameEventMessage) {\n                            this.StateMachine.changeState(State.GAME_EVENT);\n                        }\n                        else if (message.type == _common_messages__WEBPACK_IMPORTED_MODULE_1__.MessageType.GameEndsMessage) {\n                            this.StateMachine.changeState(State.GAME_ENDS);\n                        }\n                        else {\n                            wrong_state();\n                        }\n                        let ifrm = this.window.document.createElement(\"iframe\");\n                        ifrm.setAttribute(\"src\", \"message.data.src\");\n                        ifrm.style.width = \"640px\";\n                        ifrm.style.height = \"480px\";\n                        document.body.appendChild(ifrm);\n                    }\n                },\n                GAME_ENDS: {\n                    Run: function (message) {\n                        console.log(\"on GAME_ENDS\");\n                        if (message.type == _common_messages__WEBPACK_IMPORTED_MODULE_1__.MessageType.PartyEndsMessage) {\n                            this.StateMachine.changeState(State.PARTY_ENDS);\n                        }\n                        else if (message.type == _common_messages__WEBPACK_IMPORTED_MODULE_1__.MessageType.GameBeginsMassage) {\n                            this.StateMachine.changeState(State.GAME_BEGINS);\n                        }\n                        else {\n                            wrong_state();\n                        }\n                    }\n                },\n                PARTY_ENDS: {\n                    Run: function (message) {\n                        console.log(\"on PARTY_ENDS\");\n                        if (message.type == _common_messages__WEBPACK_IMPORTED_MODULE_1__.MessageType.PartyBeginsMessage) {\n                            this.StateMachine.changeState(State.PARTY_BEGINS);\n                        }\n                        else {\n                            wrong_state();\n                        }\n                    }\n                },\n            },\n            dispatch(message) {\n                console.log(\"dispatching message\");\n                const action = this.transitions[this.curr_state][\"Run\"];\n                action.apply(this, message);\n            },\n            changeState(newState) {\n                this.curr_state = newState;\n                console.log(\"State changed!\");\n            },\n            start() {\n                this.transitions[this.curr_state][\"Init\"].apply(this.StateMachine);\n            }\n        };\n        this.id = id_;\n        this.uri = uri_;\n        this.window = window_;\n        console.log(window_);\n        for (let state_ in this.StateMachine.transitions) {\n            if (typeof this.StateMachine.transitions[state_] == 'function') {\n                continue;\n            }\n            for (let func_ in this.StateMachine.transitions[state_]) {\n                if (typeof this.StateMachine.transitions[state_][func_] == 'function') {\n                    this.StateMachine.transitions[state_][func_] =\n                        this.StateMachine.transitions[state_][func_].bind(this);\n                }\n            }\n        }\n    }\n}\nconsole.log(\"Hello world!\");\nlet controller = new Controller(\"1\", \"1\", '1');\ncontroller.StateMachine.start();\ncontroller.StateMachine.changeState(State.GAME_BEGINS);\ncontroller.StateMachine.dispatch(new _common_messages__WEBPACK_IMPORTED_MODULE_1__.GameBeginsMessage('0', \"Flappy bird\", 'arcade'));\n\n\n//# sourceURL=webpack://@pocket-party/game-framework/./src/controller/controller.ts?");

/***/ }),

/***/ "./src/controller/FlappyBird-JavaScript/flappyBird.js":
/*!************************************************************!*\
  !*** ./src/controller/FlappyBird-JavaScript/flappyBird.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("module.exports = __webpack_require__.p + \"1ec39a7b32504fcf61f7.js\";\n\n//# sourceURL=webpack://@pocket-party/game-framework/./src/controller/FlappyBird-JavaScript/flappyBird.js?");

/***/ }),

/***/ "./src/controller/index.js":
/*!*********************************!*\
  !*** ./src/controller/index.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _controller_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./controller.ts */ \"./src/controller/controller.ts\");\n/* harmony import */ var _common_messages_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/messages.ts */ \"./src/common/messages.ts\");\n\n\n\nvar newWin = window.open(\"about:blank\", \"hello\", \"width=1000,height=1000\");\nconsole.log(newWin);\n\nlet controller = new _controller_ts__WEBPACK_IMPORTED_MODULE_0__.Controller(\"1\", \"1\", newWin);\ncontroller.StateMachine.changeState(_controller_ts__WEBPACK_IMPORTED_MODULE_0__.State.GAME_BEGINS);\ncontroller.StateMachine.dispatch(new _common_messages_ts__WEBPACK_IMPORTED_MODULE_1__.GameBeginsMessage('0', \"Flappy bird\", 'arcade'));\n\n\n//# sourceURL=webpack://@pocket-party/game-framework/./src/controller/index.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	(() => {
/******/ 		var scriptUrl;
/******/ 		if (__webpack_require__.g.importScripts) scriptUrl = __webpack_require__.g.location + "";
/******/ 		var document = __webpack_require__.g.document;
/******/ 		if (!scriptUrl && document) {
/******/ 			if (document.currentScript)
/******/ 				scriptUrl = document.currentScript.src
/******/ 			if (!scriptUrl) {
/******/ 				var scripts = document.getElementsByTagName("script");
/******/ 				if(scripts.length) scriptUrl = scripts[scripts.length - 1].src
/******/ 			}
/******/ 		}
/******/ 		// When supporting browsers where an automatic publicPath is not supported you must specify an output.publicPath manually via configuration
/******/ 		// or pass an empty string ("") and set the __webpack_public_path__ variable from your code to use your own logic.
/******/ 		if (!scriptUrl) throw new Error("Automatic publicPath is not supported in this browser");
/******/ 		scriptUrl = scriptUrl.replace(/#.*$/, "").replace(/\?.*$/, "").replace(/\/[^\/]+$/, "/");
/******/ 		__webpack_require__.p = scriptUrl;
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		__webpack_require__.b = document.baseURI || self.location.href;
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"main": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		// no on chunks loaded
/******/ 		
/******/ 		// no jsonp function
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/controller/index.js");
/******/ 	
/******/ })()
;