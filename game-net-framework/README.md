# Pocket Party Game Framework

## Protocol
The Pocket Party Protocol, aka 3P, aka Trip describes communication between client and servers during a single game session. The protocol is event based.

### Proxy
The server and client communicate through a proxy. That way it will be easier to implement
party-related, but not game-related features such as joining of new players during a party session,
party starts and etc. (#1)

The server-to-proxy protocol is built on top of gRPC.
The client-to-proxy protocol __will__ be built ot top of Websockets. (#2)

In the future the proxy will be replaced by the Party Manager, which handles the whole party session. Accepting new clients, handling intermediate game scores and
communicating to GameFactories. Further the __Party Manager__ will be reffered to as __proxy__.

### Message types
The protocol supports three kinds of messages
* **NewSession**. Sent from client to websocket on stream open. Used to pass player data.
* **Game Event**. Used to exchange game events between client and server. Each game event is handled by its corresponding event handler. Event handlers are selected by the events tag.
* **Scores**. Sent from server to client on stream close. Is used to pass final data about a given game session.

You can view the details in the [proto file](https://gitlab.com/pocket-party/game-net-framework/-/blob/master/proto/trip.proto).

## Framework
The framework is divided in two parts: the client and the server.

### Server
A servers handles a single game session. You may create your own game server by extending the abstract BServer. You need to implement the init method and event handlers. 
The init method will be triggered on a game session start. It's used to send out first messages to the client. 
An event handler must accept a single game event. The event's tag is specified by the `event(tag)` decorator factory.

### Game Factory
The Game Factory is a type-wrapper, that implements the server-to-proxy gRPC communication. The Game Factory can handle multiple game sessions at once. To wrap your 
Game Server into a Game Factory you just pass it as a singe template argument. Note that your Server __must__ extend the BServer class.

### Client
Still at discussion stage.

## Running the server
`yarn server`

## Installation
To install the framework add the follwing lines to your `.npmrc` config:
```js
"@pocket-party:registry"="https://gitlab.com/api/v4/packages/npm/"
"//gitlab.com/api/v4/packages/npm/:_authToken"="${GITLAB_AUTH_TOKEN}"
'//gitlab.com/api/v4/projects/25010299/packages/npm/:_authToken'="${GITLAB_AUTH_TOKEN}"
```
Then run:
```sh
GITLAB_AUTH_TOKEN=<GITLAB ACCESS TOKEN> yarn add @pocket-party/game-framework
```
